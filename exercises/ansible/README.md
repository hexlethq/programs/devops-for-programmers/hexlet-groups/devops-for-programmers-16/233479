# Ansible

Настроим Vagrant-окружение используя [Ansible Provisioning](https://www.vagrantup.com/docs/provisioning/ansible) и запустим внутри него веб-сервер Caddy, который будет обрабатывать запросы для микрофреймворка Fastify.

## Ссылки

* [Ansible Documentation](https://docs.ansible.com/ansible/latest/index.html)
* [Caddy](https://github.com/caddyserver/caddy)

## Задачи

1. Ansible: Установите git, make, postgres
2. Ansible: Залейте в postgres базу данных [pg-dump-example](https://github.com/hexlet-components/pg-dump-example). Для работы Ansible-модуля postgresql_user нужно установить пакет [psycopg2](https://docs.ansible.com/ansible/2.8/modules/postgresql_user_module.html#requirements)
3. Ansible: Установите [Node.js](https://github.com/nodesource/distributions#installation-instructions) и сервер [Caddy](https://caddyserver.com/docs/install) с помощью [shell-скриптов](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/script_module.html).
4. Ansible: Выставьте сервер наружу на портах 8080 для HTTP и 4430 для HTTPS.
5. Вручную: Установите [fastify](https://github.com/fastify/fastify#quick-start). Запустите Fastify и сделайте так, чтобы он был доступен снаружи Vagrant через Caddy.

```sh
vagrant ssh
cd /vagrant
caddy reload # подгружаем наш Caddyfile
npm init -y fastify
npm install
FASTIFY_ADDRESS=0.0.0.0 npm run dev # проверяем что все работает
# open localhost:8080
```

Убедитесь в том что все задачи выполняются идемпотентно.
